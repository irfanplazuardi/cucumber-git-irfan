Feature: Login functionality on logon page of application

  Scenario: Verification of Login Button works
    Given Open the chrome and launch the application
    When Enter the username and password
    And Click login Button
    Then User will be directed to managers page