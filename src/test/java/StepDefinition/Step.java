package StepDefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Step {
    WebDriver driver;

    @Given("^Open the chrome and launch the application$")
    public void Open_the_chrome_and_launch_the_application() throws Throwable {
        //System.out.println("this step open the chrome browser and launch the application");
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://demo.guru99.com/v4");
    }

    @When("^Enter the username and password$")
    public void Enter_the_username_and_password() throws Throwable {
        //System.out.println("This step enter the username and password on the login page");
        driver.findElement(By.name("uid")).sendKeys("mngr418202");
        driver.findElement(By.name("password")).sendKeys("jArUmUb");
    }

    @And("^Click login Button$")
    public void Click_login_button() throws Throwable {
        //System.out.println("This step is to click login button");
        driver.findElement(By.xpath("/html/body/form/table/tbody/tr[3]/td[2]/input[1]")).click();
    }

    @Then("^User will be directed to managers page$")
    public void User_will_be_directed_to_managers_page() throws Throwable {
        //System.out.println("This step will show Manger's Page");
        String welcome_page = driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[2]/td/marquee")).getText();
        Assert.assertTrue("Welcome To Manager's Page of Guru99 Bank", welcome_page.contains("Welcome To Manager's Page of Guru99 Bank"));
    }

}
